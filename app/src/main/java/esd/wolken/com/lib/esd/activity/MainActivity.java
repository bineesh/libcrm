package esd.wolken.com.lib.esd.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.List;
import java.util.logging.Logger;

import esd.wolken.com.lib.esd.R;
import esd.wolken.com.lib.esd.beans.CRMLogin;
import esd.wolken.com.lib.esd.beans.TrackTicket;
import esd.wolken.com.lib.esd.listener.OnUpdateLoginResultListener;
import esd.wolken.com.lib.esd.listener.OnUpdateTrackTicketListener;
import esd.wolken.com.lib.esd.listener.OnUserUpdateListener;
import esd.wolken.com.lib.esd.util.CRMVolleySingleton;
import esd.wolken.com.lib.esd.util.SharedMethods;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Boolean isFabOpen = false;
    private FloatingActionButton mParentFab, mTicketFab, mChatFab;
    private Animation mFabOpen;
    private Animation mFabClose;
    private Animation mRotateForward;
    private Animation mRotateBackward;
    private ProgressBar mProgressBar;
    private RelativeLayout mSearchLayout;
    private RecyclerView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.app_title);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mSearchLayout = (RelativeLayout) findViewById(R.id.search_layout);
        callSearchActivity();
        initializeFabButton();

        callApi();
    }

    private void callSearchActivity() {

        mSearchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SearchArticleActivity.class));
            }
        });

    }


    private void initializeFabButton() {
        mParentFab = (FloatingActionButton) findViewById(R.id.fab);
        mTicketFab = (FloatingActionButton) findViewById(R.id.ticketFab);
        mChatFab = (FloatingActionButton) findViewById(R.id.fab2);
        mFabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_anim_open);
        mFabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_anim_open);
        mRotateForward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        mRotateBackward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        mParentFab.setOnClickListener(this);
        mTicketFab.setOnClickListener(this);
        mChatFab.setOnClickListener(this);
    }

    public void animateFAB() {

        if (isFabOpen) {
            mParentFab.startAnimation(mRotateBackward);
            mTicketFab.clearAnimation();
            mChatFab.clearAnimation();
            mTicketFab.setVisibility(View.INVISIBLE);
            mChatFab.setVisibility(View.INVISIBLE);
            isFabOpen = false;
        } else {
            mParentFab.startAnimation(mRotateForward);
            mTicketFab.startAnimation(mFabOpen);
            mChatFab.startAnimation(mFabOpen);
            isFabOpen = true;

        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab:
                animateFAB();
                break;
            case R.id.ticketFab:
                startActivity(new Intent(MainActivity.this, QueryActivity.class));

                break;
            case R.id.fab2:
                break;

            default:
                break;
        }
    }



    private void callApi() {
        if (!SharedMethods.hasLoggedIn(this)) {
            CRMVolleySingleton.getInstance(this).getApi().login(this, new OnUpdateLoginResultListener() {
                @Override
                public void onUpdateLoginResult(CRMLogin login) {
                    if (login == null) {
                        return;
                    }
                    mProgressBar.setVisibility(View.GONE);
                    if (login.getStatus() == 0) {
                        SharedMethods.storeCompanyIdIdInPref(MainActivity.this, login.getCompanyId());
                        SharedMethods.storeSessionIdInPref(MainActivity.this, login.getSessionId());
                        CRMVolleySingleton.getInstance(MainActivity.this).getApi().createUser(MainActivity.this, "bineesh@wolkensoftware.com", "Bineesh", new OnUserUpdateListener() {
                            @Override
                            public void onUserUpdate(boolean isSuccess) {
                                CRMVolleySingleton.getInstance(MainActivity.this).getApi().getTrackTickets(MainActivity.this, new OnUpdateTrackTicketListener() {
                                    @Override
                                    public void onUpdateTrackTicket(List<TrackTicket> trackTickets) {

                                        Logger.getLogger("Success"+trackTickets.toString());

                                    }
                                });

                            }
                        });
                    } else {
                        SharedMethods.showMyAlert(MainActivity.this, "you are using an invalid api key", "Login failed");
                    }
                }
            });
        } else {
            CRMVolleySingleton.getInstance(MainActivity.this).getApi().getTrackTickets(MainActivity.this, new OnUpdateTrackTicketListener() {
                @Override
                public void onUpdateTrackTicket(List<TrackTicket> trackTickets) {

                    Logger.getLogger("Success"+trackTickets.toString());

                }
            });
        }
    }
}
