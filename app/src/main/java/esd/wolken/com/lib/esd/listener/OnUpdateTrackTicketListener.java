package esd.wolken.com.lib.esd.listener;

import java.util.List;

import esd.wolken.com.lib.esd.beans.TrackTicket;

/**
 * Created by bineesh on 4/12/17.
 */

public interface OnUpdateTrackTicketListener {
    public void onUpdateTrackTicket(List<TrackTicket> trackTickets);
}
