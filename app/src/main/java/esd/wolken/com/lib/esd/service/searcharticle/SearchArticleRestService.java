package esd.wolken.com.lib.esd.service.searcharticle;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.beans.SearchArticleResponse;
import esd.wolken.com.lib.esd.util.JsonUtil;
import esd.wolken.com.lib.esd.util.SharedValues;

/**
 * Created by sagar on 1/19/2018.
 */

public class SearchArticleRestService extends StringRequest {
    private static final String TAG = SearchArticleRestService.class.getCanonicalName();
    private String parameter;
    private Context mContext;
    private String data = null;

    public SearchArticleRestService(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, String mSearchQueryText) {
        super(method, url, listener, errorListener);
        mContext = context;
        parameter = mSearchQueryText;

    }


    /*Get the network response Data*/
    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse networkResponse) {

        data = new String(networkResponse.data);
        return Response.success(data, getCacheEntry());

    }

   // Parse the network response data
   /* private SearchArticleResponse jsonParser(String data) {
        SearchArticleResponse SearchArticleResponse = JsonUtil.fromJson(data, new SearchArticleResponse());
        return SearchArticleResponse;
    }*/


    /*Add the header*/
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headerparams = new HashMap<>();
        headerparams.put("Content-Type", "application/x-www-form-urlencoded");
        return headerparams;
    }

    /*Add the url parameters*/
    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        HashMap<String, String> params = new HashMap<>();
        params.put("searchType", "MATCHING_TEXT");
        params.put("host", SharedValues.SEARCHARTICLEDOMAINNAME);
        params.put("queryStr",parameter);
        return params;
    }
}
