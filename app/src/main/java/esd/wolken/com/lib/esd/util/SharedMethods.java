package esd.wolken.com.lib.esd.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import esd.wolken.com.lib.esd.activity.SearchArticleActivity;

/**
 * Created by bineesh on 27/11/17.
 */

public class SharedMethods {

    static ProgressDialog dialog = null;

    Context mContext;

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(SharedValues.PREF_NAME, 0);

    }

    public static SharedPreferences.Editor getEditor(SharedPreferences preferences) {
        return preferences.edit();
    }

    public static void storeSessionIdInPref(Context context, String sessionId) {
        SharedPreferences.Editor editor = getEditor(getPreferences(context));
        editor.putString(SharedValues.STORE_SESSION_ID, sessionId);
        editor.commit();
    }

    public static void storeCompanyIdIdInPref(Context context, int sessionId) {
        SharedPreferences.Editor editor = getEditor(getPreferences(context));
        editor.putInt(SharedValues.STORE_COMPANY_ID, sessionId);
        editor.commit();
    }

    public static void storeUserIdInPref(Context context, int userId) {
        SharedPreferences.Editor editor = getEditor(getPreferences(context));
        editor.putInt(SharedValues.STORE_USER_ID, userId);
        editor.commit();
    }

    public static void storeEmailIdInPref(Context context, String emailId) {
        SharedPreferences.Editor editor = getEditor(getPreferences(context));
        editor.putString(SharedValues.STORE_EMAIL_ID, emailId);
        editor.commit();
    }

    public static void storeUserNameInPref(Context context, String name) {
        SharedPreferences.Editor editor = getEditor(getPreferences(context));
        editor.putString(SharedValues.STORE_NAME, name);
        editor.commit();
    }

    public static Integer getUserIdInPref(Context context) {
        return getPreferences(context).getInt(SharedValues.STORE_USER_ID, 0);
    }

    public static String getSessionInPref(Context context) {
        return getPreferences(context).getString(SharedValues.STORE_SESSION_ID, "");
    }

    public static Integer getCompanyIdInPref(Context context) {
        return getPreferences(context).getInt(SharedValues.STORE_COMPANY_ID, 0);
    }

    public static boolean hasLoggedIn(Context context) {
        return getCompanyIdInPref(context).intValue() != 0
                && getSessionInPref(context) != "";
    }

    public static void showMyAlert(final Context context, String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.setMessage(message);
        alert.show();
    }


    public static void showProgressDilogue(Context context) {

        dialog = new ProgressDialog(context);
        dialog.setMessage("Searching...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

    public static void hideProgressDialogue(Context context) {

        dialog.dismiss();
    }


}
