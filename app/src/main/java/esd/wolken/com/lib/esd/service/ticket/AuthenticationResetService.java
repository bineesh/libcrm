package esd.wolken.com.lib.esd.service.ticket;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.beans.ResponseHolder;
import esd.wolken.com.lib.esd.service.CRMService;

/**
 * Created by bineesh on 28/11/17.
 */

public class AuthenticationResetService extends CRMService<String> {

    public AuthenticationResetService(Context context, int method, String Url, String requestObj, Response.Listener<String> responseListener, Response.ErrorListener errorListener) {
        super(context, method, Url, "request="+requestObj, responseListener, errorListener);

    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse networkResponse) {
         ResponseHolder responseHolder = getDataFromResponse(networkResponse);
        String data = responseHolder.getData().toString();
        return Response.success(data, getCacheEntry());
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        String credentials =getAuthentication("cUryateIkSe","bustTeWolken");
        headers.put("Authorization", credentials);
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        return headers;
    }

    private String getAuthentication(String username, String password) {
        String credentials = String.format("%s:%s", username, password);
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        return auth;
    }
}
