package esd.wolken.com.lib.esd.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchArticleForJsonArray  implements Parcelable{


    public SearchArticleForJsonArray(){

    }

    //@SerializedName("articleId")
    String articleId;
   // @SerializedName("name")
    String name;
   // @SerializedName("shortDesc")
    String shortDesc;

    //@SerializedName("viewMoreArticleUrl")
    String viewMoreArticleUrl;

  //  @SerializedName("status")
    String status;

   // @SerializedName("createdTime")
    String createdTime;

    protected SearchArticleForJsonArray(Parcel in) {
        articleId = in.readString();
        name = in.readString();
        shortDesc = in.readString();
        viewMoreArticleUrl = in.readString();
        status = in.readString();
        createdTime = in.readString();
    }

    public static final Creator<SearchArticleForJsonArray> CREATOR = new Creator<SearchArticleForJsonArray>() {
        @Override
        public SearchArticleForJsonArray createFromParcel(Parcel in) {
            return new SearchArticleForJsonArray(in);
        }

        @Override
        public SearchArticleForJsonArray[] newArray(int size) {
            return new SearchArticleForJsonArray[size];
        }
    };

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getViewMoreArticleUrl() {
        return viewMoreArticleUrl;
    }

    public void setViewMoreArticleUrl(String viewMoreArticleUrl) {
        this.viewMoreArticleUrl = viewMoreArticleUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(articleId);
        dest.writeString(name);
        dest.writeString(shortDesc);
        dest.writeString(viewMoreArticleUrl);
        dest.writeString(status);
        dest.writeString(createdTime);
    }
}
