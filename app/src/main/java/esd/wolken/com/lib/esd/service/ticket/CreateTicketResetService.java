package esd.wolken.com.lib.esd.service.ticket;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.service.CRMService;

import static android.content.ContentValues.TAG;

/**
 * Created by bineesh on 28/11/17.
 */

public class CreateTicketResetService extends CRMService<String> {
    MultipartEntity entity = new MultipartEntity();
    private final File mFilePart;
    private String parameter;
    private String mAuthToken;

    public CreateTicketResetService(Context context, int method, String Url, String requestObj, File file, String authToken, Response.Listener<String> responseListener, Response.ErrorListener errorListener) {
        super(context, method, Url, requestObj, responseListener, errorListener);
        mFilePart = file;
        parameter = requestObj;
        this.context = context;
//        mAuthToken = authToken;
        buildMultipartEntity();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
//        params.put("token", mAuthToken);
        return params;
    }

    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
            String entityContentAsString = new String(bos.toByteArray());
            Log.e("volley", entityContentAsString);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String jsonStrig = new String(response.data);
        Log.d(TAG, "" + jsonStrig);
        return Response.success(jsonStrig, getCacheEntry());
    }

    private void buildMultipartEntity() {
        if (mFilePart != null) {
            entity.addPart("attachment", new FileBody(mFilePart));
        }
        try {
            entity.addPart("request", new StringBody(parameter));
        } catch (UnsupportedEncodingException e) {
            VolleyLog.e("UnsupportedEncodingException");
        }
    }


}
