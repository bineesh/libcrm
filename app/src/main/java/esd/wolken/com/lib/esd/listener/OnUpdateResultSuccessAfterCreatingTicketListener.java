package esd.wolken.com.lib.esd.listener;

/**
 * Created by sagar on 11/30/2017.
 */

public interface OnUpdateResultSuccessAfterCreatingTicketListener {

    void onUpdateResultSuccessAfterCreatingTicket(Boolean successfullyCreated, String message);


}
