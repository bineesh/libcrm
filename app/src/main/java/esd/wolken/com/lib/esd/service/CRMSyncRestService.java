package esd.wolken.com.lib.esd.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import esd.wolken.com.lib.esd.beans.CRMLogin;
import esd.wolken.com.lib.esd.beans.SearchArticleForJsonArray;
import esd.wolken.com.lib.esd.beans.SearchArticleResponse;
import esd.wolken.com.lib.esd.beans.TrackTicket;
import esd.wolken.com.lib.esd.listener.OnAuthenticationResultUpdateListener;
import esd.wolken.com.lib.esd.listener.OnUpdateArticleSearchListener;
import esd.wolken.com.lib.esd.listener.OnUpdateLoginResultListener;
import esd.wolken.com.lib.esd.listener.OnUpdateResultSuccessAfterCreatingTicketListener;
import esd.wolken.com.lib.esd.listener.OnUpdateTrackTicketListener;
import esd.wolken.com.lib.esd.listener.OnUserUpdateListener;
import esd.wolken.com.lib.esd.service.createuser.CreateUserResetService;
import esd.wolken.com.lib.esd.service.login.LoginRestService;
import esd.wolken.com.lib.esd.service.searcharticle.SearchArticleRestService;
import esd.wolken.com.lib.esd.service.ticket.AuthenticationResetService;
import esd.wolken.com.lib.esd.service.ticket.CreateTicketResetService;
import esd.wolken.com.lib.esd.service.trackticket.TrackTicketRestService;
import esd.wolken.com.lib.esd.util.SharedMethods;

/**
 * Created by bineesh on 27/11/17.
 */

public class CRMSyncRestService {
    private final RequestQueue mQueue;

    public CRMSyncRestService(RequestQueue mRequestQueue) {
        mQueue = mRequestQueue;
    }

    List<SearchArticleResponse> mSearchArticleResponse;

    public void login(Context context, final OnUpdateLoginResultListener listener) {
        LoginRestService loginResetService = new LoginRestService(context, Request.Method.POST, "/helpCenterLogin", "", new Response.Listener<CRMLogin>() {
            @Override
            public void onResponse(CRMLogin login) {
                if (listener != null) {
                    listener.onUpdateLoginResult(login);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                listener.onUpdateLoginResult(null);
            }
        });

        loginResetService.setRetryPolicy(new DefaultRetryPolicy(60000,
                1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(loginResetService);
    }


    public void getAuthentication(Context context, String emailId, final OnAuthenticationResultUpdateListener listener) {
        JSONObject jsonobject = null;
        try {
            jsonobject = new JSONObject();
            jsonobject.accumulate("from", emailId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AuthenticationResetService authenticationResetService = new AuthenticationResetService(context, Request.Method.POST, "/getcreateticketaccess", jsonobject.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("AuthenticationReset", "Response" + response.getBytes());
                JSONObject json = null;
                try {
                    json = new JSONObject(response);
                    if (json != null) {
                        String status = json.getString("status");
                        if (status.equals("success")) {
                            String token = json.getString("token");
                            if (listener != null) {
                                listener.onAuthenticationResultUpdate(token);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Logger.getLogger("Authentication Error"+volleyError.getLocalizedMessage());

            }
        });
        authenticationResetService.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(authenticationResetService);
    }


    public void createTicket(final Context context, String submitQueriesList, File attachedFile, String token, final OnUpdateResultSuccessAfterCreatingTicketListener listener) {


        CreateTicketResetService createTicketResetService = new CreateTicketResetService(context, Request.Method.POST, "/inapptktcreation", submitQueriesList, attachedFile, token, new Response.Listener<String>() {
            @Override
            public void onResponse(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    boolean success = jsonObject.getString("status").equals("success") ? true : false;
                    String message = jsonObject.getString("message");
                    listener.onUpdateResultSuccessAfterCreatingTicket(success, message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                listener.onUpdateResultSuccessAfterCreatingTicket(false, "message");


            }

        });
        createTicketResetService.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(createTicketResetService);
    }

    public void createUser(final Context context, final String emailId, final String name, final OnUserUpdateListener onUserUpdateListener) {

        final CreateUserResetService createUserResetService = new CreateUserResetService(context, Request.Method.POST, "/createHelpCenterUser", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // response
                Log.d("Response", response);
                try {

                    JSONObject json = new JSONObject(response);

                    JSONObject restResponseStatus = json.getJSONObject("restResponseStatus");
                    boolean isSuccess = restResponseStatus.getBoolean("isSuccess");
                    if (isSuccess) {
                        JSONObject resposeObject = json.getJSONObject("data");
                        if (resposeObject != null) {
                            SharedMethods.storeUserIdInPref(context, resposeObject.getInt("userId"));
                            SharedMethods.storeEmailIdInPref(context, emailId);
                            SharedMethods.storeUserNameInPref(context, name);
                        }
                    }
                    onUserUpdateListener.onUserUpdate(isSuccess);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }//end of response
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Logger.getLogger("Create User Failed"+error.getLocalizedMessage());

            }
        }, name, emailId);
        createUserResetService.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(createUserResetService);
    }


    /*method for searching the articles*/
    public void getSearchArticleData(final Context context, final String searchArticleQueryText, final OnUpdateArticleSearchListener listener) {

        //APPEND THE URL PARAMETERS
        final SearchArticleRestService searchArticleRestService = new SearchArticleRestService(context, Request.Method.POST, "https://knowledge.wolkencare.com/elasticsearch/es/getarticlefromstring", new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                List<SearchArticleForJsonArray> list = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(response); //ORIGINAL RESPONSE

                    String successMessage = jsonObject.getString("statusType");
                    if (successMessage.equals("OK")) {

                        JSONObject searchArray1 = jsonObject.getJSONObject("entity");
                        JSONObject searcArray2 = searchArray1.getJSONObject("data");
                        JSONArray jsonArray = searcArray2.getJSONArray("jsonArray");
                         for (int i = 0; i < jsonArray.length(); i++) {

                            SearchArticleForJsonArray searchArticleForJsonArray = new SearchArticleForJsonArray();
                            JSONObject object2 = jsonArray.getJSONObject(i);
                            String mArticleName  = object2.getString("name");
                            searchArticleForJsonArray.setName(mArticleName);
                            String mShortDesc = object2.getString("shortDesc");
                            searchArticleForJsonArray.setShortDesc(mShortDesc);
                            String mCreatedAt = object2.getString("createdTime");
                            searchArticleForJsonArray.setCreatedTime(mCreatedAt);
                            String mViewMoreArticleUrl = object2.getString("viewMoreArticleUrl");
                            searchArticleForJsonArray.setViewMoreArticleUrl(mViewMoreArticleUrl);


                            list.add(searchArticleForJsonArray);

                            Log.d("SearchArray!!Results", "onResponse: " + list);

                            listener.onUpdateArticleSelected(successMessage, list);

                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }//end of response
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
        }, searchArticleQueryText);
        searchArticleRestService.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(searchArticleRestService);
    }


    public void getTrackTickets(final Context context, OnUpdateTrackTicketListener listener) {

        TrackTicketRestService trackTicketRestService = new TrackTicketRestService(context, Request.Method.POST, "/searchTicket", null, new Response.Listener<TrackTicket>() {
            @Override
            public void onResponse(TrackTicket trackTicket) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        trackTicketRestService.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(trackTicketRestService);
    }
}