package esd.wolken.com.lib.esd.beans;

/**
 * Created by bineesh on 27/11/17.
 */

public class ResponseHolder {

    private String data;
    private int error;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
