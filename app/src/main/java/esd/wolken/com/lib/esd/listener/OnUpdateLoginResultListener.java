package esd.wolken.com.lib.esd.listener;

import esd.wolken.com.lib.esd.beans.CRMLogin;

/**
 * Created by bineesh on 27/11/17.
 */

public interface OnUpdateLoginResultListener {
    void onUpdateLoginResult(CRMLogin login);
}
