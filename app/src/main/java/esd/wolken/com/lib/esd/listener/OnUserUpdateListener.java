package esd.wolken.com.lib.esd.listener;

/**
 * Created by bineesh on 4/12/17.
 */

public interface  OnUserUpdateListener {
    public void onUserUpdate(boolean isSuccess);
}
