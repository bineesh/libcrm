package esd.wolken.com.lib.esd.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by bineesh on 27/11/17.
 */

public class SystemConfig {
    public static String getAppKey(Context context) {
        String appKey = null;
        try {
            ApplicationInfo aplicationMetadata = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = aplicationMetadata.metaData;
            appKey = bundle.getString("com.wolken.crm.help.desk.api.key");
        } catch (PackageManager.NameNotFoundException e) {
            // TODO: handle exception
            Log.e("HELPCENTER_APP_KEY", "failed to load metadata Name not found");
        } catch (NullPointerException e) {
            // TODO: handle exception
            Log.e("HELPCENTER_APP_KEY", "failed to load metatdata null pointer");

        }
        return appKey;

    }


    public static String getVersionName(Context context) {
        String versionName = null;
        try {
            versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;

    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


//    public static String getPhoneNumber(Context context) {
//        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        String mPhoneNumber = tMgr.getLine1Number();
//        return mPhoneNumber;
//    }

    public static String getNetworkProvider(Context context) {
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getNetworkOperatorName();
        return mPhoneNumber;
    }

    public static String getNetworkType(Context context) {
        Activity act = (Activity) context;
        String network_type = "UNKNOWN";//maybe usb reverse tethering
        NetworkInfo active_network = ((ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (active_network != null && active_network.isConnectedOrConnecting()) {
            if (active_network.getType() == ConnectivityManager.TYPE_WIFI) {
                network_type = "WIFI";
            } else if (active_network.getType() == ConnectivityManager.TYPE_MOBILE) {
                network_type = ((ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo().getSubtypeName();
            }
        }
        return network_type;
    }

    public static String getLanguage() {
        return Locale.getDefault().getDisplayLanguage();
    }

    public static String getCountryCode(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }


    public static String getBatteryStatus(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        return isCharging ? "Charging" : "Not Charging";
    }

    public static String getBatteryLevel(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float) scale;
        return "" + (int) batteryPct * 100 + "%";
    }

    public static String getFreeInternalSpace(Context context) {
        long freeBytesInternal = new File(context.getFilesDir().getAbsoluteFile().toString()).getFreeSpace();
        return convertToGB(freeBytesInternal);
    }

    public static String getFreeSDCardSpace(Context context) {
        long freeBytesExternal = new File(context.getExternalFilesDir(null).toString()).getFreeSpace();
        return convertToGB(freeBytesExternal);
    }

    public static String getTotalInternalSpace(Context context) {
        long freeBytesInternal = new File(context.getFilesDir().getAbsoluteFile().toString()).getTotalSpace();
        return convertToGB(freeBytesInternal);
    }

    public static String getTotalSDCardSpace(Context context) {
        long freeBytesExternal = new File(context.getExternalFilesDir(null).toString()).getTotalSpace();
        return convertToGB(freeBytesExternal);
    }

    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    public static String convertToGB(long value) {
        double kb = value / (1024);
        double mb = kb / (1024);
        double gb = Double.parseDouble(new DecimalFormat("##.##").format(mb / (1024)));
        return "" + gb + " GB";
    }
}
