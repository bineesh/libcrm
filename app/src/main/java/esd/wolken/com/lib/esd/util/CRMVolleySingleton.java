package esd.wolken.com.lib.esd.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

import esd.wolken.com.lib.esd.service.CRMSyncRestService;

import static com.android.volley.toolbox.Volley.newRequestQueue;

/**
 * Created by bineesh on 27/11/17.
 */

public class CRMVolleySingleton {
    private static final String DEFAULT_CACHE_DIR = "volley";
    private static CRMVolleySingleton mInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private CRMSyncRestService resetService;

    private CRMVolleySingleton(Context context) {
        mRequestQueue = newRequestQueue(context);
        resetService = new CRMSyncRestService(mRequestQueue);

    }

    public static synchronized CRMVolleySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CRMVolleySingleton(context);
        }
        return mInstance;
    }

    public CRMSyncRestService getApi() {
        return resetService;
    }

    public void stopService(){
        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                // do I have to cancel this?
                return true; // -> always yes
            }
        });
    }

}
