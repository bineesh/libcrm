package esd.wolken.com.lib.esd.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import java.util.List;
import java.util.logging.Logger;

import esd.wolken.com.lib.esd.R;
import esd.wolken.com.lib.esd.adapters.SearchArticleResponseAdapter;
import esd.wolken.com.lib.esd.beans.SearchArticleForJsonArray;
import esd.wolken.com.lib.esd.listener.OnUpdateArticleSearchListener;
import esd.wolken.com.lib.esd.util.CRMVolleySingleton;
import esd.wolken.com.lib.esd.util.ItemOffsetDecoration;
import esd.wolken.com.lib.esd.util.SharedMethods;
import esd.wolken.com.lib.esd.util.SharedValues;
import esd.wolken.com.lib.esd.util.SimpleDividerItemDecoration;

public class SearchArticleActivity extends AppCompatActivity {
    public static final String TAG = SearchArticleActivity.class.getSimpleName();


    private RecyclerView mSearchArticleRv;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText mInputQueryEditText;
    private String mSearchQueryString = null;
    // private ImageView mSearchView;
    private ImageView mCloseSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_article);
        mInputQueryEditText = findViewById(R.id.search_article_editText);

        // mSearchView = findViewById(R.id.search_article_searchView);
        mCloseSearch = findViewById(R.id.close_search_article_iv);
        mSearchQueryString = mInputQueryEditText.getText().toString();
        Log.d(TAG, "mSearchQueryString: " + mSearchQueryString);
        mInputQueryEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hitSearch();
                    Log.d(TAG, "onEditorAction: " + mSearchQueryString);
                    return true;
                }
                return false;
            }
        });



       /* mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitSearch();
            }
        });*/

        mCloseSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInputQueryEditText.getText().toString().length() > 0) {
                    mInputQueryEditText.setText("");

                } else {

                    Logger.getLogger("Cant close the search");
                }
            }
        });
        Toolbar myToolbar = findViewById(R.id.toolbar_search_article);
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back);


        mSearchArticleRv = findViewById(R.id.search_list_rv);
        mSearchArticleRv.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(SearchArticleActivity.this, R.dimen.item_offset);

        mSearchArticleRv.addItemDecoration(itemDecoration);
        mSearchArticleRv.setLayoutManager(mLayoutManager);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchArticleActivity.this, MainActivity.class));
            }
        });


    }


    private void hitSearch() {

        SharedMethods.showProgressDilogue(SearchArticleActivity.this);
        CRMVolleySingleton.getInstance(this).getApi().getSearchArticleData(getApplicationContext(), mInputQueryEditText.getText().toString(), new OnUpdateArticleSearchListener() {


            @Override
            public void onUpdateArticleSelected(String statusType, List<SearchArticleForJsonArray> searchArticleResponseArrayList) {

                if (statusType.equals("OK")) {
                    SharedMethods.hideProgressDialogue(SearchArticleActivity.this);
                    Log.d(TAG, "onUpdateArticleSelected: " + searchArticleResponseArrayList);
                   // mSearchArticleRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    mAdapter = new SearchArticleResponseAdapter(searchArticleResponseArrayList, SearchArticleActivity.this);
                    mSearchArticleRv.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                } else if (statusType.isEmpty() || !statusType.equals("OK")) {

                    Toast.makeText(SearchArticleActivity.this, "Sorry Search Article Results not found!!!", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem sendItem = menu.findItem(R.id.action_send);
        sendItem.setVisible(false);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        //searchView.setFocusable(true);
        searchView.setIconified(false);

        searchView.setQueryHint("Search For the Article");
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(new ComponentName(this, SearchArticleActivity.class)));

        //searchView.setVisibility(View.INVISIBLE);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_send:
                item.setVisible(false);

        }
        return super.onOptionsItemSelected(item);
    }


    /*@Override
    public void onClick(View v) {

        Log.d(TAG, "onClick: "+mSearchView.getQuery());
        String mSearchArticleString = String.valueOf(mSearchView.getQuery());
        showProgressDilogue();

        CRMVolleySingleton.getInstance(this).getApi().getSearchArticleData(getApplicationContext(), mSearchArticleString, new OnUpdateArticleSearchListener() {


            @Override
            public void onUpdateArticleSelected(String statusType, List<SearchArticleForJsonArray> searchArticleResponseArrayList) {

                if (statusType.equals("OK")) {
                    hideProgressDialogue();
                    Log.d(TAG, "onUpdateArticleSelected: " + searchArticleResponseArrayList);
                    mSearchArticleRv.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
                    mAdapter = new SearchArticleResponseAdapter(searchArticleResponseArrayList, SearchArticleActivity.this);
                    mSearchArticleRv.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                } else if (statusType.isEmpty() || !statusType.equals("OK")) {

                    Toast.makeText(SearchArticleActivity.this, "Sorry Search Article Results not found!!!", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }*/

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


}
