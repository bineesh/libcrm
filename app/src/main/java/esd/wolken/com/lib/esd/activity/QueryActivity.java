package esd.wolken.com.lib.esd.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import esd.wolken.com.lib.esd.R;
import esd.wolken.com.lib.esd.beans.SubmitQuery;
import esd.wolken.com.lib.esd.listener.OnUpdateResultSuccessAfterCreatingTicketListener;
import esd.wolken.com.lib.esd.util.CRMVolleySingleton;
import esd.wolken.com.lib.esd.util.JsonUtil;
import esd.wolken.com.lib.esd.util.RealPathUtil;
import esd.wolken.com.lib.esd.util.SharedMethods;
import esd.wolken.com.lib.esd.util.UtilityForImageAndGallery;

public class QueryActivity extends AppCompatActivity {

    public static final String TAG = QueryActivity.class.getSimpleName();
    private TextView mName;
    private EditText edName;
    private TextView mEmail;
    private EditText edEmail;
    private TextView mDescription;
    private EditText edDescription;
    private Button mSend;
    private ImageView mAttachmentView;
    private int REQUEST_CAMERA = 1, SELECT_FILE = 0;
    private String userChoosenTask;
    private TextView mAttachedTextView;
    private ImageView mRemoveAttachment;
    private RelativeLayout mAttachedLayout;
    private ImageView mFinalImageViewAttached;
    private RelativeLayout mImageAndAttachmentText;
    private ScrollView mScrollView;

    private List<SubmitQuery> addSubmitQueryDetails;
    private String selectedImagePath = null;
    private File getTheFinalSelectedImagePath;
    private Uri mCapturedImageURI = null;
    private String imgPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();

        setContentView(R.layout.activity_query);
        addSubmitQueryDetails = new ArrayList<>();
        Toolbar myToolbar = findViewById(R.id.toolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.app_titleBar_text);
        mName = (TextView) findViewById(R.id.tv_name);
        edName = (EditText) findViewById(R.id.ed_name_box);
        mEmail = (TextView) findViewById(R.id.tv_email);
        edEmail = (EditText) findViewById(R.id.ed_email_box);
        mDescription = (TextView) findViewById(R.id.tv_description);
        edDescription = (EditText) findViewById(R.id.ed_description_box);
        mAttachmentView = (ImageView) findViewById(R.id.add_attachment);
        mRemoveAttachment = (ImageView) findViewById(R.id.close_icon);
        mAttachedLayout = (RelativeLayout) findViewById(R.id.attachment_layout);
        mFinalImageViewAttached = (ImageView) findViewById(R.id.attachment_final_image);
        mImageAndAttachmentText = (RelativeLayout) findViewById(R.id.layout_image_and_attachment);
        mAttachedTextView = (TextView) findViewById(R.id.attachment_text);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QueryActivity.this, MainActivity.class));
            }
        });


        mAttachedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseTheCameraOrGallery();

            }
        });


        mRemoveAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAttachedLayout.setVisibility(View.VISIBLE);
                mAttachmentView.setVisibility(View.VISIBLE);
                mFinalImageViewAttached.setVisibility(View.INVISIBLE);

                mFinalImageViewAttached.setImageBitmap(null);
                mAttachedTextView.setVisibility(View.VISIBLE);
                //mAttachmentView.setImageBitmap(null);
                mRemoveAttachment.setVisibility(View.GONE);
            }
        });

    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void chooseTheCameraOrGallery() {
        mAttachedTextView.setVisibility(View.VISIBLE);
        mFinalImageViewAttached.setVisibility(View.GONE);

        final CharSequence[] items = {"Take Photo", "Choose from Library"};
        AlertDialog.Builder builder = new AlertDialog.Builder(QueryActivity.this);
        builder.setTitle("Select Below");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = UtilityForImageAndGallery.checkPermission(QueryActivity.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }


    public String getImagePath() {
        return imgPath;
    }

    private void galleryIntent() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK && resultCode != RESULT_CANCELED) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                try {
                    onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void onCaptureImageResult(Intent data) throws IOException {
        if (null != data) {
            Bundle extras = data.getExtras();
            Bitmap thumbnail = (Bitmap) extras.get("data"); //gives me the path

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);
            /*Lets create the path*/
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
            //
            getTheFinalSelectedImagePath = new File(image.getAbsolutePath());
            //getTheFinalSelectedImagePath = new File(getRealAttachmentPathNew((image.getAbsolutePath())));

            mFinalImageViewAttached.setMaxHeight(150);
            mFinalImageViewAttached.setMaxWidth(150);

            mFinalImageViewAttached.setImageBitmap(thumbnail);
            mFinalImageViewAttached.setVisibility(View.VISIBLE);

            mAttachmentView.setVisibility(View.GONE);
            mAttachedTextView.setVisibility(View.GONE);
            mRemoveAttachment.setVisibility(View.VISIBLE);
        } else {

            Toast.makeText(this, "Sorry!!! Image not Captured!!!", Toast.LENGTH_SHORT).show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        Bitmap newselectedImage = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                newselectedImage = getResizedBitmap(bm, 400);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        getTheFinalSelectedImagePath = new File(getRealAttachmentPath(data.getData()));
        if (newselectedImage != null)
            mFinalImageViewAttached.setImageBitmap(newselectedImage);
        mFinalImageViewAttached.setVisibility(View.VISIBLE);

        mAttachmentView.setVisibility(View.GONE);
        mAttachedTextView.setVisibility(View.GONE);
        mRemoveAttachment.setVisibility(View.VISIBLE);

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        menu.findItem(R.id.search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_send:
                if (TextUtils.isEmpty(edName.getText().toString())) {
                    edName.setError(getString(R.string.error_name));
                } else if (isValidEmail(edEmail.getText().toString())) {
                    edEmail.setError(getString(R.string.error_email));
                } else if (TextUtils.isEmpty(edDescription.getText().toString())) {
                    edDescription.setError(getString(R.string.error_description));
                }

                SubmitQuery submitQuery = new SubmitQuery();
                submitQuery.setUser_name(edName.getText().toString());
                submitQuery.setUser_email(edEmail.getText().toString());
                submitQuery.setDesc(edDescription.getText().toString());
                submitQuery.setCompanyId(SharedMethods.getCompanyIdInPref(this));

                String requestObject = JsonUtil.toJson(submitQuery);
                CRMVolleySingleton.getInstance(QueryActivity.this).getApi().createTicket(QueryActivity.this, requestObject, getTheFinalSelectedImagePath, "", new OnUpdateResultSuccessAfterCreatingTicketListener() {
                    @Override
                    public void onUpdateResultSuccessAfterCreatingTicket(Boolean successfullyCreated, String message) {

                        if (successfullyCreated) {
                            edName.setText("");
                            edEmail.setText("");
                            edDescription.setText("");
                            mAttachedLayout.setVisibility(View.VISIBLE);
                            mAttachmentView.setVisibility(View.VISIBLE);
                            mFinalImageViewAttached.setVisibility(View.INVISIBLE);

                            mFinalImageViewAttached.setImageBitmap(null);
                            mAttachedTextView.setVisibility(View.VISIBLE);
                            mRemoveAttachment.setVisibility(View.GONE);
                            Toast.makeText(QueryActivity.this, "Successfully created the ticket!!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                break;

            case R.id.search:

            default:
                return super.onOptionsItemSelected(item);

        }
        return super.onOptionsItemSelected(item);

    }

    private boolean isValidEmail(String emailInput) {

        return TextUtils.isEmpty(emailInput) && Patterns.EMAIL_ADDRESS.matcher(emailInput).matches();

    }


    @Override
    protected void onResume() {
        super.onResume();
        //chooseTheCameraOrGallery();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public int getFilesCount(Context context) {

        return 1;
    }

    private String getRealAttachmentPath(Uri data) {
        String realPath = null;
        if (Build.VERSION.SDK_INT < 11)
            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data);
        else if (Build.VERSION.SDK_INT < 19)
            realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data);
        else
            realPath = RealPathUtil.getRealPathFromURI_API19(this, data);
        return realPath;
    }


}