package esd.wolken.com.lib.esd.listener;

/**
 * Created by bineesh on 28/11/17.
 */

public interface OnAuthenticationResultUpdateListener {
    public void onAuthenticationResultUpdate(String token);
}
