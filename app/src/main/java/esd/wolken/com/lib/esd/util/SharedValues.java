package esd.wolken.com.lib.esd.util;

import esd.wolken.com.lib.esd.service.searcharticle.SearchArticleRestService;

/**
 * Created by bineesh on 27/11/17.
 */

public class SharedValues {
    public static final String BASE_URL = "https://selfservice.wolkencare.com/issuetracker/services/service";
    //    public static final String BASE_URL ="http://192.168.0.109:8080/issuetracker/services/service";
    public static final String PREF_NAME = "com.lib.crm.sharedPreferences";
    public static final String STORE_SESSION_ID = "com.lib.crm.session_id";

    public static final String STORE_COMPANY_ID = "com.lib.crm.company_id";
    public static final String STORE_USER_ID = "com.lib.crm.user_id";
    public static final String STORE_EMAIL_ID = "com.lib.crm.email_id";
    public static final String STORE_NAME = "com.lib.crm.name";
    public static final String SEARCHARTICLEDOMAINNAME = "selfservice.wolkencare.com";
    public static final String SEARCHARTICLEKEYSUCCESS = "statusType";
    }
