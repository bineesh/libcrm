package esd.wolken.com.lib.esd.service.trackticket;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.beans.ResponseHolder;
import esd.wolken.com.lib.esd.beans.TrackTicket;
import esd.wolken.com.lib.esd.service.CRMService;
import esd.wolken.com.lib.esd.util.SharedMethods;

/**
 * Created by bineesh on 1/12/17.
 */

public class TrackTicketResetService extends CRMService<TrackTicket>{


    public TrackTicketResetService(Context context, int method, String Url, String requestObj, Response.Listener<TrackTicket> responseListener, Response.ErrorListener errorListener) {
        super(context, method, Url, requestObj, responseListener, errorListener);
    }

    @Override
    protected Response<TrackTicket> parseNetworkResponse(NetworkResponse networkResponse) {
        ResponseHolder responseHolder = getDataFromResponse(networkResponse);
        String data = responseHolder.getData().toString();
        return Response.success(getJson(data), getCacheEntry());
    }

    private TrackTicket getJson(String data) {
        TrackTicket trackTicket=new TrackTicket();
        return trackTicket;
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/x-www-form-urlencoded");
        return super.getHeaders();
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("timestamp", ""+0);
        params.put("userId",""+SharedMethods.getUserIdInPref(context));
        return params;
    }
}
