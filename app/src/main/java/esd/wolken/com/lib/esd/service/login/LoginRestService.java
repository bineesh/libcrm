package esd.wolken.com.lib.esd.service.login;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.google.gson.Gson;

import java.util.Map;

import esd.wolken.com.lib.esd.beans.CRMLogin;
import esd.wolken.com.lib.esd.beans.ResponseHolder;
import esd.wolken.com.lib.esd.service.CRMService;
import esd.wolken.com.lib.esd.util.JsonUtil;

import static esd.wolken.com.lib.esd.util.SystemConfig.getAppKey;

/**
 * Created by bineesh on 27/11/17.
 */

public class LoginRestService extends CRMService<CRMLogin>{
    private static final String TAG =LoginRestService.class.getCanonicalName();

    public LoginRestService(Context context, int method, String Url, String requestObj, Response.Listener<CRMLogin> responseListener, Response.ErrorListener errorListener) {
        super(context, method, Url, requestObj, responseListener, errorListener);
    }

    @Override
    protected Response<CRMLogin> parseNetworkResponse(NetworkResponse networkResponse) {
        ResponseHolder responseHolder = getDataFromResponse(networkResponse);
        String data = responseHolder.getData().toString();
        Log.d(TAG, "" + responseHolder.getData());
        return Response.success(jsonParser(data), getCacheEntry());
    }

    private CRMLogin jsonParser(String data) {
        Gson gson=new Gson();
        CRMLogin login= JsonUtil.fromJson(data,new CRMLogin());
        return login;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        params.put("appKey",""+getAppKey(context));
        params.put("Content-Type", "application/json; charset=utf-8");
        return super.getHeaders();
    }
}
