package esd.wolken.com.lib.esd.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bineesh on 28/11/17.
 */


public class SubmitQuery {
    private String user_name;
    private String user_email;
    private String desc;
    private String host;
    private int companyId;


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
