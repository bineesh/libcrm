package esd.wolken.com.lib.esd.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import esd.wolken.com.lib.esd.R;
import esd.wolken.com.lib.esd.activity.SearchArticleWebView;
import esd.wolken.com.lib.esd.beans.SearchArticleForJsonArray;
import esd.wolken.com.lib.esd.beans.SearchArticleResponse;

/**
 * Created by sagar on 1/23/2018.
 */

public class SearchArticleResponseAdapter extends RecyclerView.Adapter<SearchArticleResponseAdapter.ViewHolder> {


    private List<SearchArticleForJsonArray> searchArticleResponses = new ArrayList<>();

    private Context mContext;

    public SearchArticleResponseAdapter(List<SearchArticleForJsonArray> searchArticleResponses, Context mContext) {
        this.searchArticleResponses = searchArticleResponses;
        this.mContext = mContext;
    }

    @Override
    public SearchArticleResponseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_article_response_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchArticleResponseAdapter.ViewHolder holder, int position) {
        if (searchArticleResponses != null) {
            holder.mArticleName.setText(searchArticleResponses.get(position).getName());
            holder.mCreatedAt.setText(searchArticleResponses.get(position).getCreatedTime());
            holder.mArticleShortdesc.setText(searchArticleResponses.get(position).getShortDesc());
            holder.mArticleUrl.setText(searchArticleResponses.get(position).getViewMoreArticleUrl());
            //String position1 = (searchArticleResponses.get(position).getViewMoreArticleUrl());
            final SearchArticleForJsonArray searchUrlList = searchArticleResponses.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent gotoWebViewActivity = new Intent(mContext, SearchArticleWebView.class);
                    gotoWebViewActivity.putExtra("SEARCH_URL",searchUrlList);
                    gotoWebViewActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(gotoWebViewActivity);
                }
            });
            //holder.mArticleUrl.setTag(position1);

        }
    }

    @Override
    public int getItemCount() {
        return searchArticleResponses.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mArticleName;
        TextView mArticleShortdesc;
        TextView mCreatedAt;
        TextView mArticleUrl;

        public ViewHolder(View itemView) {
            super(itemView);
            mArticleName = (itemView).findViewById(R.id.article_name);
            mArticleShortdesc = (itemView).findViewById(R.id.article_short_desc);
            mCreatedAt = (itemView).findViewById(R.id.article_created_at);
            mArticleUrl = (itemView).findViewById(R.id.search_article_url);


        }


    }
}
