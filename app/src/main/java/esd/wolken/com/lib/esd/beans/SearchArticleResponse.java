package esd.wolken.com.lib.esd.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sagar on 1/19/2018.
 */

public class SearchArticleResponse {

    private String statusType;
    private String entity;
    private String message;
    private String data;
    private String unArchivedCount;
    private String resultSize;
    private String maxResultSize;
    private String list;
    private String totalCount;
    private String jsonArray;
    private String articleId;
    private String name;
    private String shortDesc;
    private String viewMoreArticleUrl;
    private String status;
    private String createdTime;
    private String archivedCount;
    private String entityType;
    private String metadata;
    private String Content_Type;
    private String type;
    private String subtype;
    private String parameters;
    private String wildcardType;
    private String wildcardSubtype;


    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUnArchivedCount() {
        return unArchivedCount;
    }

    public void setUnArchivedCount(String unArchivedCount) {
        this.unArchivedCount = unArchivedCount;
    }

    public String getResultSize() {
        return resultSize;
    }

    public void setResultSize(String resultSize) {
        this.resultSize = resultSize;
    }

    public String getMaxResultSize() {
        return maxResultSize;
    }

    public void setMaxResultSize(String maxResultSize) {
        this.maxResultSize = maxResultSize;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(String jsonArray) {
        this.jsonArray = jsonArray;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getViewMoreArticleUrl() {
        return viewMoreArticleUrl;
    }

    public void setViewMoreArticleUrl(String viewMoreArticleUrl) {
        this.viewMoreArticleUrl = viewMoreArticleUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getArchivedCount() {
        return archivedCount;
    }

    public void setArchivedCount(String archivedCount) {
        this.archivedCount = archivedCount;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getContent_Type() {
        return Content_Type;
    }

    public void setContent_Type(String content_Type) {
        Content_Type = content_Type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getWildcardType() {
        return wildcardType;
    }

    public void setWildcardType(String wildcardType) {
        this.wildcardType = wildcardType;
    }

    public String getWildcardSubtype() {
        return wildcardSubtype;
    }

    public void setWildcardSubtype(String wildcardSubtype) {
        this.wildcardSubtype = wildcardSubtype;
    }
}





