package esd.wolken.com.lib.esd.service.createuser;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.util.SharedMethods;
import esd.wolken.com.lib.esd.util.SharedValues;

/**
 * Created by bineesh on 4/12/17.
 */

public class CreateUserResetService extends StringRequest {
    private String mUserName;
    private String mEmailId;
    private Context mContext;

    public CreateUserResetService(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, String name, String emailId) {
        super(method, SharedValues.BASE_URL + url, listener, errorListener);
        mUserName = name;
        mEmailId = emailId;
        mContext = context;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("companyId", "" + SharedMethods.getCompanyIdInPref(mContext));
        Log.i("companyId", "" + SharedMethods.getCompanyIdInPref(mContext));
        params.put("sessionId", "" + SharedMethods.getSessionInPref(mContext));
        Log.i("sessionId", "" + SharedMethods.getSessionInPref(mContext));
        params.put("Content-Type", "application/x-www-form-urlencoded");
        return params;
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("userName", mUserName);
        params.put("emailId", mEmailId);
        return params;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse networkResponse) {
        String data = new String(networkResponse.data);
        return Response.success(data, getCacheEntry());
    }
}
