package esd.wolken.com.lib.esd.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ZoomControls;

import esd.wolken.com.lib.esd.R;
import esd.wolken.com.lib.esd.beans.SearchArticleForJsonArray;
import esd.wolken.com.lib.esd.util.SharedMethods;

public class SearchArticleWebView extends AppCompatActivity {


    private WebView mSearchArtcileWebView;
    private ProgressDialog dialog = null;
    private ProgressDialog progressDialog = null;

    private android.support.v7.widget.Toolbar mSearchToolBar;

    private SearchArticleForJsonArray searchArticleForJsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_article_web_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = ProgressDialog.show(SearchArticleWebView.this, "", "Please wait, your article is being loaded...", true);

        mSearchArtcileWebView = findViewById(R.id.load_search_article_web_view);
        // Configure related browser settings
        mSearchArtcileWebView.getSettings().setLoadsImagesAutomatically(true);
        mSearchArtcileWebView.getSettings().setJavaScriptEnabled(true);
        mSearchArtcileWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        // Enable responsive layout
        mSearchArtcileWebView.getSettings().setUseWideViewPort(true);
// Zoom out if the content width is greater than the width of the viewport
        mSearchArtcileWebView.getSettings().setLoadWithOverviewMode(true);
        mSearchArtcileWebView.getSettings().setSupportZoom(true);
        mSearchArtcileWebView.getSettings().setTextZoom(170);
        mSearchArtcileWebView.getSettings().setBuiltInZoomControls(true); // allow pinch to zooom
        mSearchArtcileWebView.getSettings().setDisplayZoomControls(false);

// disable the default zoom controls on the page
        mSearchArtcileWebView.setWebViewClient(new MyBrowser());
        // Load the initial URL

        searchArticleForJsonArray = getIntent().getParcelableExtra("SEARCH_URL");
        SharedMethods.showProgressDilogue(SearchArticleWebView.this);
        if (null != searchArticleForJsonArray)
            SharedMethods.hideProgressDialogue(SearchArticleWebView.this);
        mSearchArtcileWebView.loadUrl(searchArticleForJsonArray.getViewMoreArticleUrl());
    }


    // Manages the behavior when URLs are loaded
    public class MyBrowser extends WebViewClient {
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            progressDialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }
}
