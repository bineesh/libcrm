package esd.wolken.com.lib.esd.listener;

import java.util.List;

import esd.wolken.com.lib.esd.beans.SearchArticleForJsonArray;

/**
 * Created by sagar on 1/19/2018.
 */

public interface OnUpdateArticleSearchListener {

    void onUpdateArticleSelected(String statusType, List<SearchArticleForJsonArray> searchArticleResponseArrayList);
}
