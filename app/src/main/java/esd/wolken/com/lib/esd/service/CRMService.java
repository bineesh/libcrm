package esd.wolken.com.lib.esd.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonRequest;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import esd.wolken.com.lib.esd.beans.ResponseHolder;
import esd.wolken.com.lib.esd.util.SharedMethods;
import esd.wolken.com.lib.esd.util.SharedValues;

/**
 * Created by Bineesh on 1/31/2017.
 */

public abstract class CRMService<T> extends JsonRequest<T> {
    private static final String TAG = CRMService.class.getName();

    public Context context;
    protected Map<String, String> params = new HashMap<String, String>();

    public CRMService(Context context, int method, String Url, String requestObj, Response.Listener<T> responseListener, Response.ErrorListener errorListener) {
        super(method, SharedValues.BASE_URL + Url, requestObj, responseListener, errorListener);
        this.context = context;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        params.put("companyId", "" + SharedMethods.getCompanyIdInPref(context));
        Log.i("companyId", "" + SharedMethods.getCompanyIdInPref(context));
        params.put("sessionId", "" + SharedMethods.getSessionInPref(context));
        Log.i("sessionId", "" + SharedMethods.getSessionInPref(context));
        return params;
    }

    public ResponseHolder getDataFromResponse(NetworkResponse response) {
        ResponseHolder responseHolder = new ResponseHolder();
        try {
            String data = new String(response.data);
            JSONObject jsonObject = new JSONObject(data);
            if (jsonObject.has("data")) {

                JSONObject dataObject = jsonObject.optJSONObject("data");

                if (dataObject != null) {
                    responseHolder.setData(dataObject.toString());

                } else {

                    JSONArray array = jsonObject.optJSONArray("data");
                    responseHolder.setData(array.toString());
                }
            }else{
                responseHolder.setData(data);
            }
        } catch (Exception e) {
            Log.e(TAG, "getDataFromResponse: ", e);
        }

        return responseHolder;
    }
}
