package esd.wolken.com.lib.esd.util;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by mahesh on 2/2/2017.
 */

public class JsonUtil {

    public static < E > E fromJson(String json,E obj) {
        Gson gson = new Gson();
        E res = (E) gson.fromJson(json,obj.getClass());
        return res;
    }
    public static String toJson(Object obj){
        Gson gson=new Gson();
        return gson.toJson(obj);
    }
    public static String toJson(Object obj,List<String> removeKeys){
        Gson gson=new Gson();
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(gson.toJson(obj));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (String key : removeKeys) {
            jsonObject.remove(key);
        }
        return jsonObject.toString();
    }
}
